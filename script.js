
function union(setA, setB) {
    return new Set([...setA, ...setB]);
}
function intersection(setA, setB) {
    return new Set([...setA].filter(x => setB.has(x)));
}
function difference(setA, setB) {
    return new Set([...setA].filter(x => !setB.has(x)));
}
function symmetricDifference(setA, setB) {
    let diffA = difference(setA, setB);
    let diffB = difference(setB, setA);
    return new Set([...diffA, ...diffB]);
}
function validateInput(input) {
    input = input.replace(/\s+/g, '');
    let values = [];
    let nums = input.split(',');
    for (let num of nums) {
        if (num !== '') {
            values.push(Number(num));
        }
    }
    if (values.some(isNaN)) {
        throw new Error("Неправильний формат введення. Введіть цілі числа, розділені комою.");
    }
    return new Set(values);
}
function sortSet(set) {
    return Array.from(set).sort((a, b) => a - b);
}

function calculate() {
    try {
        let setA = validateInput(document.getElementById("setA").value);
        let setB = validateInput(document.getElementById("setB").value);
        let operation = document.getElementById("operation").value;
        let setC;

        switch (operation) {
            case '∪':
                setC = union(setA, setB);
                break;
            case '∩':
                setC = intersection(setA, setB);
                break;
            case '∆':
                setC = symmetricDifference(setA, setB);
                break;
            case '\\':
                setC = difference(setA, setB);
                break;
            default:
                console.error("Невідома операція");
                return;
        }

        let sortedSetC = sortSet(setC);
        document.getElementById("setC").textContent = sortedSetC.join(', ');
        document.getElementById("errorMessage").textContent = "";
        updateVennDiagram(operation);
    }catch (error) {
        document.getElementById("setC").textContent = "";
        document.getElementById("errorMessage").textContent = error.message;
    }
}
function updateVennDiagram(operation) {
    const imageSrc = {
        '∪': 'img/Venn0111.svg',
        '∩': 'img/Venn0001.svg',
        '∆': 'img/Venn0110.svg',
        '\\': 'img/Venn0010.svg',
    };
    document.getElementById("vennDiagram").src = imageSrc[operation];
  }
